# Lumberyard Starter Game(Unofficial User Guide)
![amazon-lumberyard](https://user-images.githubusercontent.com/18353476/28592053-ae35f67a-713c-11e7-9d2b-9fab8de0c94c.jpg)

Install Lumberyard: https://aws.amazon.com/lumberyard/

Lumberyard Documentation: https://docs.aws.amazon.com/lumberyard/latest/userguide/setting-up-system-requirements.html

Amazon GameDev Tutorials: https://gamedev.amazon.com/forums/tutorials

An Introduction to AWS Lumberyard Game Development: https://www.packtpub.com/books/content/introduction-aws-lumberyard-game-development

# Lumberyard Support for macOS(Preview release)

macOS Support Documentation: https://docs.aws.amazon.com/lumberyard/latest/userguide/osx-intro.html

# Lumberyard Support for Linux(Preview release)

https://docs.aws.amazon.com/lumberyard/latest/userguide/linux-intro.html

# System Requirements

Lumberyard requires the following hardware and software:

Windows 7 64-bit or Later

3GHz minimum quad-core processor

8 GB RAM minimum

2 GB minimum DX11 or later compatible video card

Nvidia driver version 368.81 or AMD driver version 16.15.2211 graphics card

60 GB minimum of free disk space

One or both of the following: (required to compile Lumberyard Editor and tools)

    Visual Studio 2013 Update 4 or later
    
    Visual Studio 2015 Update 3 or later
    
 # Setup and Configure Projects in Lumberyard
 
![1_launcher](https://user-images.githubusercontent.com/18353476/28295300-864421c4-6b14-11e7-8771-40a598086499.png)
![2_setupgems](https://user-images.githubusercontent.com/18353476/28333786-f2a032d8-6bad-11e7-97db-59fd6675c37d.png)
![startergame](https://user-images.githubusercontent.com/18353476/28592987-b807bc58-713f-11e7-9e0b-af9f8049dc37.PNG)
![startergame](https://user-images.githubusercontent.com/18353476/28298168-9e83a612-6b26-11e7-8943-070c6ff4f2d2.jpg)
![starter2](https://user-images.githubusercontent.com/18353476/28487938-beef8d24-6e52-11e7-93ad-3c9d95b88c30.PNG)

 # Game controls
 To start the game, press Ctrl+G. Use the following keyboard keys and mouse controls:

To move forward, strafe left, move backward, and strafe right, press the W, A, S, and D keys, respectively.
To look around, move the mouse.
To shoot, click the primary mouse button.
To toggle weapon selection, click the secondary mouse button.
To toggle time of day, press the F key.
To exit the game, press Esc.
If you are running Starter Game in the editor and Jack the robot dies, press Esc to exit the game.
